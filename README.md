
## Start application

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Clone repository
2. Start "npm install (or yarn add)"
3. Use npm run android (yarn run android) to run app (Current version not tested for ios)

---

## Application features

This application allows you to find movies, series and episodes by title and release year.

* To start searching, enter the title to input field and press "search"
* To apply search filtering, select type and enter year of the product (optional)
* To reset the search and filter values, click "Reset Filters"
* To update list of recommendations, swipe down on the "Recommended" section.

---
